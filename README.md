# Rodem Good Practices

## Installation

### Docker
The easiest way to run the code for the workshop is using a docker image via
```bash
docker run -it -p 8881:8881  gitlab-registry.cern.ch/rodem-workshops/rodem-good-practices/base-python:latest bash
```
if you want to make the current folder accessible within the image you need to mount it via `-v $(pwd):<path-to-folder>`

To run a Jupyter Notebook within the Docker image, use the following command:

```bash
jupyter notebook --ip 0.0.0.0 --no-browser --port 8881
```
### Singularity
Using singularity you can simply run
```bash
singularity exec --pwd ${PWD} docker://gitlab-registry.cern.ch/rodem-workshops/rodem-good-practices/base-python:latest jupyter notebook --no-browser --port 8871
```
with the `-B` flag you can mount additional directories.

On baobab you need to load singularity before being able to use it via
```bash
module load GCC/9.3.0 Singularity/3.7.3-GCC-9.3.0-Go-1.14
```


